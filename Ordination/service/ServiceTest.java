package service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ServiceTest {
	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("0225646654", "Mogens Mogensen", 75);
		laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2, "M1");
		service = Service.getTestService();
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		Laegemiddel lm1 = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		Laegemiddel lm2 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		Patient p1 = service.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Patient p2 = service.opretPatient("Finn Madsen", "070985-1153", 83.2);
		Patient p3 = service.opretPatient("Hans Jørgensen", "050972-1233", 89.4);
		Patient p4 = service.opretPatient("Ulla Nielsen", "011064-1522", 59.9);
		Patient p5 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);

		DagligFast df1 = service.opretDagligFastOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 04), p1,
				lm1, 1, 2, 1, 2);
		DagligFast df2 = service.opretDagligFastOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 04), p2,
				lm1, 1, 2, 1, 2);
		DagligFast df3 = service.opretDagligFastOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 04), p3,
				lm1, 1, 2, 1, 2);
		DagligFast df4 = service.opretDagligFastOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 04), p4,
				lm1, 1, 2, 1, 2);
		DagligFast df5 = service.opretDagligFastOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 04), p5,
				lm2, 1, 2, 1, 2);

		assertEquals(3, service.antalOrdinationerPrVægtPrLægemiddel(60, 95, lm1));
		assertEquals(1, service.antalOrdinationerPrVægtPrLægemiddel(75, 90, lm2));

		try {
			service.antalOrdinationerPrVægtPrLægemiddel(-10, 120, lm1);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Angivne vægte skal være større end 0");
		}

		try {
			service.antalOrdinationerPrVægtPrLægemiddel(60, 40, lm1);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Startvægten må ikke være større end slutvægten");
		}

	}

	@Test
	public void opretPNOrdinationTest() {
		PN pn = service.opretPNOrdination(LocalDate.of(2016, 05, 7), LocalDate.of(2016, 05, 9), patient, laegemiddel,
				2);
		if (!patient.getOrdinationer().contains(pn)) {
			fail("PN ikke oprettet");
		}

		PN pn1 = service.opretPNOrdination(LocalDate.of(2011, 9, 14), LocalDate.of(2011, 9, 14), patient, laegemiddel,
				5);
		if (!patient.getOrdinationer().contains(pn1)) {
			fail("PN ikke oprettet");
		}

		try {
			PN pn2 = service.opretPNOrdination(LocalDate.of(2014, 3, 23), LocalDate.of(2014, 3, 20), patient,
					laegemiddel, 2);
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Startdato kan ikke være efter slutdatoen");
		}
		try {
			PN pn3 = service.opretPNOrdination(LocalDate.of(2016, 5, 7), LocalDate.of(2016, 5, 9), patient, laegemiddel,
					0);
		} catch (IllegalArgumentException i) {
			assertEquals(i.getMessage(), "Antallet skal være >0");
		}

	}

	@Test

	public void testanbefaletDosisPrDoegn() {
		Laegemiddel Paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		Patient Patient1 = new Patient("3434343434", "hans", 80);
		assertEquals(120, Patient1.getVaegt() * Paracetamol.getEnhedPrKgPrDoegnNormal(), 0.0001);

		Patient Patient2 = new Patient("3434343434", "hans", 24);
		assertEquals(24, Patient2.getVaegt() * Paracetamol.getEnhedPrKgPrDoegnLet(), 0.0001);

		Patient Patient3 = new Patient("3434343434", "hans", 120);
		assertEquals(180, Patient3.getVaegt() * Paracetamol.getEnhedPrKgPrDoegnNormal(), 0.0001);

		Patient Patient4 = new Patient("3434343434", "hans", 121);
		assertEquals(242, Patient4.getVaegt() * Paracetamol.getEnhedPrKgPrDoegnTung(), 0.0001);
	}

	@Test
	public void opretDagligFastOrdinationTest() {
		DagligFast df = service.opretDagligFastOrdination(LocalDate.of(2016, 5, 07), LocalDate.of(2016, 5, 9), patient,
				laegemiddel, 5, 5, 6, 6);
		if (!patient.getOrdinationer().contains(df)) {
			fail("Daglig fast ikke oprettet");
		}

		DagligFast df1 = service.opretDagligFastOrdination(LocalDate.of(2011, 9, 14), LocalDate.of(2011, 9, 14),
				patient, laegemiddel, 5, 5, 5, 5);

		try {
			DagligFast df2 = service.opretDagligFastOrdination(LocalDate.of(2014, 03, 23), LocalDate.of(2014, 03, 20),
					patient, laegemiddel, 1, 1, 1, 1);
		} catch (IllegalArgumentException i) {
			assertEquals(i.getMessage(), "Startdato kan ikke være efter slutdatoen");
		}
	}

	@Test
	public void opretDagligSkaevOrdination() {
		double[] antalEnheder = { 10.0, 5.0, 4.0 };
		LocalTime[] tidspunkter = { LocalTime.of(06, 00), LocalTime.of(15, 00), LocalTime.of(21, 00) };
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.of(2016, 05, 7), LocalDate.of(2016, 5, 9),
				patient, laegemiddel, tidspunkter, antalEnheder);
		if (!patient.getOrdinationer().contains(ds)) {
			fail("Daglig skæv ikke oprettet");
		}

		DagligSkaev ds1 = service.opretDagligSkaevOrdination(LocalDate.of(2011, 9, 14), LocalDate.of(2011, 9, 14),
				patient, laegemiddel, tidspunkter, antalEnheder);
		if (!patient.getOrdinationer().contains(ds1)) {
			fail("Daglig skæv ikke oprettet");
		}
		try {
			DagligSkaev ds2 = service.opretDagligSkaevOrdination(LocalDate.of(2014, 3, 23), LocalDate.of(2014, 3, 20),
					patient, laegemiddel, tidspunkter, antalEnheder);
		} catch (IllegalArgumentException i) {
			assertEquals(i.getMessage(), "Startdato kan ikke være efter slutdatoen");
		}
	}
}
