package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
		if (this.antalDage() <= 0) {
			throw new RuntimeException("Dagene er ikke korrekt angivet");
		}
		doser = new ArrayList<Dosis>();
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
	}

	public void deleteDosis(Dosis dosis) {
		doser.remove(dosis);
	}

	@Override
	public double samletDosis() {
		int antal = 0;
		for (Dosis dosis : doser) {
			antal += dosis.getAntal();
		}
		return antal * antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}
}
