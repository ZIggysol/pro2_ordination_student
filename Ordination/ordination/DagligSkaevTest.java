package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

	private Laegemiddel lm;

	@Before
	public void setUp() throws Exception {
		lm = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	}

	@Test
	public void testSamletDosis() {
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2015, 03, 02), LocalDate.of(2015, 03, 03), lm);
		for (int i = 0; i < 5; i++) {
			ds1.opretDosis(LocalTime.of(10 + i, 0), 1);
		}
		assertEquals(10, ds1.samletDosis(), 0.00001);

		DagligSkaev ds2 = new DagligSkaev(LocalDate.of(2015, 03, 02), LocalDate.of(2015, 03, 02), lm);
		for (int i = 0; i < 5; i++) {
			ds2.opretDosis(LocalTime.of(10 + i, 0), 1);
		}
		assertEquals(5, ds2.samletDosis(), 0.00001);

		DagligSkaev ds3 = new DagligSkaev(LocalDate.of(2015, 04, 16), LocalDate.of(2015, 05, 16), lm);
		for (int i = 0; i < 5; i++) {
			ds3.opretDosis(LocalTime.of(10 + i, 0), 2);
		}
		assertEquals(310, ds3.samletDosis(), 0.00001);
	}

	@Test
	public void testDoegnDosis() {
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2015, 03, 02), LocalDate.of(2015, 03, 02), lm);
		for (int i = 0; i < 5; i++) {
			ds1.opretDosis(LocalTime.of(10 + i, 0), 20);
		}
		assertEquals(100, ds1.doegnDosis(), 0.00001);

		DagligSkaev ds2 = new DagligSkaev(LocalDate.of(2015, 03, 02), LocalDate.of(2015, 07, 02), lm);
		for (int i = 0; i < 5; i++) {
			ds2.opretDosis(LocalTime.of(10 + i, 0), 4);
		}
		assertEquals(20, ds2.doegnDosis(), 0.00001);
	}

	@Test
	public void testOpretDosis() {
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2015, 03, 02), LocalDate.of(2015, 03, 02), lm);
		ds1.opretDosis(LocalTime.of(10, 00), 1);
		if (ds1.getDoser().size() != 1) {
			fail("Dose ikke oprettet");
		}
	}

}
