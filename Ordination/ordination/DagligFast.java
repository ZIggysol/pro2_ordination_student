package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private Dosis[] doser;
	private int givetDosis = 0;

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemidler, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemidler);
		this.doser = new Dosis[4];
		doser[0] = new Dosis(LocalTime.of(06, 00), morgenAntal);
		doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
		doser[2] = new Dosis(LocalTime.of(18, 00), aftenAntal);
		doser[3] = new Dosis(LocalTime.of(00, 00), natAntal);
	}

	@Override
	public double samletDosis() {
		int dagsDosis = 0;
		for (int i = 0; i < 4; i++) {
			dagsDosis += doser[i].getAntal();
		}
		int dage = antalDage();
		return dagsDosis * dage;
	}

	@Override
	public double doegnDosis() {
		int dage = antalDage();

		return samletDosis() / dage;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

	public Dosis[] getDoser() {
		return doser.clone();
	}
}
