package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	}

	@Test
	public void testSamletDosis() {
		DagligFast df = new DagligFast(LocalDate.of(2015, 03, 02), LocalDate.of(2015, 03, 03), laegemiddel, 2, 1, 2, 1);
		assertEquals(12, df.samletDosis(), 0.1);

		DagligFast df1 = new DagligFast(LocalDate.of(2015, 03, 02), LocalDate.of(2015, 03, 02), laegemiddel, 2, 1, 2,
				1);
		assertEquals(6, df1.samletDosis(), 0.10);

		DagligFast df2 = new DagligFast(LocalDate.of(2015, 4, 16), LocalDate.of(2015, 5, 16), laegemiddel, 2, 4, 2, 3);
		assertEquals(341, df2.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		DagligFast dagligFast = new DagligFast(LocalDate.of(2015, 5, 1), LocalDate.of(2015, 6, 21), laegemiddel, 0, 1,
				1, 0);
		assertEquals(2, dagligFast.doegnDosis(), 0.001);

		DagligFast dagligFast1 = new DagligFast(LocalDate.of(2014, 01, 01), LocalDate.of(2014, 02, 01), laegemiddel, 2,
				3, 2, 3);
		assertEquals(10, dagligFast1.doegnDosis(), 0.001);
	}

}
