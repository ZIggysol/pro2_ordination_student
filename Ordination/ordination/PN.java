package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel Laegemiddel, double antalEnheder) {
		super(startDen, slutDen, Laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	private double antalEnheder;
	private int sumAfDoser = 0;
	private ArrayList<LocalDate> doseringsDatoer = new ArrayList<>();
	private LocalDate førstGivne = null;
	private LocalDate sidstGivne = null;

	public ArrayList<LocalDate> getDoseringsDatoer() {
		return doseringsDatoer;
	}

	public void setSumAfDoser(int sumAfDoser) {
		this.sumAfDoser = sumAfDoser;
	}

	public LocalDate getFørstGivne() {
		return førstGivne;
	}

	public LocalDate getSidstGivne() {
		return sidstGivne;
	}

	public void setFørstGivne(LocalDate førstGivne) {
		this.førstGivne = førstGivne;
	}

	public void setSidstGivne(LocalDate sidstGivne) {
		this.sidstGivne = sidstGivne;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if ((givesDen.isAfter(getStartDen()) || givesDen.isEqual(getStartDen()))
				&& (givesDen.isBefore(getSlutDen()) || givesDen.isEqual(getSlutDen()))) {
			sumAfDoser++;
			doseringsDatoer.add(givesDen);
			if (førstGivne == null || givesDen.isBefore(førstGivne)) {
				førstGivne = givesDen;
			}
			if (sidstGivne == null || givesDen.isAfter(sidstGivne)) {
				sidstGivne = givesDen;
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public double doegnDosis() {
		if (førstGivne == null && sidstGivne == null) {
			return 0;
		} else {
			return samletDosis() / (ChronoUnit.DAYS.between(førstGivne, sidstGivne) + 1);
		}
	}

	@Override
	public double samletDosis() {
		return getAntalEnheder() * getAntalGangeGivet();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return sumAfDoser;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
