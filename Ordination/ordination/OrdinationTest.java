package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.time.LocalDate;

import org.junit.Test;

public class OrdinationTest {

	@Test
	public void testAntalDage() {
		Laegemiddel Paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		DagligFast DF1 = new DagligFast(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 12), Paracetamol, 2, 2, 2, 2);
		assertEquals(1, DF1.antalDage());

		DagligFast DF2 = new DagligFast(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 9, 2), Paracetamol, 2, 2, 2, 2);
		assertEquals(24, DF2.antalDage());

		DagligFast DF3 = new DagligFast(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 13), Paracetamol, 2, 2, 2, 2);
		assertNotEquals(1, DF3.antalDage());

	}

}
