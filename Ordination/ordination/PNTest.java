package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

public class PNTest {

	@Test
	public void testsamletDosis() {
		Laegemiddel Paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		PN pn1 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 2);
		pn1.givDosis(LocalDate.of(2015, 8, 11));
		pn1.givDosis(LocalDate.of(2015, 8, 14));
		pn1.givDosis(LocalDate.of(2015, 8, 16));
		pn1.givDosis(LocalDate.of(2015, 8, 16));
		pn1.givDosis(LocalDate.of(2015, 8, 19));
		assertEquals(10, pn1.samletDosis(), 0.00001);

		PN pn2 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 25), Paracetamol, 4);
		pn2.givDosis(LocalDate.of(2015, 8, 12));
		pn2.givDosis(LocalDate.of(2015, 8, 13));
		pn2.givDosis(LocalDate.of(2015, 8, 14));
		pn2.givDosis(LocalDate.of(2015, 8, 15));
		pn2.givDosis(LocalDate.of(2015, 8, 16));
		pn2.givDosis(LocalDate.of(2015, 8, 17));
		pn2.givDosis(LocalDate.of(2015, 8, 18));
		pn2.givDosis(LocalDate.of(2015, 8, 19));
		pn2.givDosis(LocalDate.of(2015, 8, 20));
		pn2.givDosis(LocalDate.of(2015, 8, 21));

		assertEquals(40, pn2.samletDosis(), 0.00001);

		PN pn3 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 2);
		assertEquals(0, pn3.samletDosis(), 0.00001);
	}

	@Test
	public void testdoegnDosis() {
		Laegemiddel Paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		PN pn1 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 3);
		pn1.givDosis(LocalDate.of(2015, 8, 12));
		pn1.givDosis(LocalDate.of(2015, 8, 12));
		pn1.givDosis(LocalDate.of(2015, 8, 14));
		pn1.givDosis(LocalDate.of(2015, 8, 14));
		pn1.givDosis(LocalDate.of(2015, 8, 13));
		assertEquals(5, pn1.doegnDosis(), 0.00001);

		PN pn2 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 4);
		pn2.givDosis(LocalDate.of(2015, 8, 12));
		pn2.givDosis(LocalDate.of(2015, 8, 14));
		pn2.givDosis(LocalDate.of(2015, 8, 17));
		assertEquals(2, pn2.doegnDosis(), 0.00001);
	}

	@Test
	public void testgetAntalGangeGivet() {
		Laegemiddel Paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		PN pn1 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 3);
		pn1.givDosis(LocalDate.of(2015, 8, 12));
		pn1.givDosis(LocalDate.of(2015, 8, 13));
		pn1.givDosis(LocalDate.of(2015, 8, 14));
		pn1.givDosis(LocalDate.of(2015, 8, 15));
		assertEquals(4, pn1.getAntalGangeGivet(), 0.00001);

	}

	@Test
	public void testgivDosis() {
		Laegemiddel Paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		PN pn1 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 3);
		assertTrue(pn1.givDosis(LocalDate.of(2015, 8, 12)));
		assertEquals(1, pn1.getAntalGangeGivet());
		assertEquals(1, pn1.getDoseringsDatoer().size());

		PN pn2 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 3);
		assertFalse(pn2.givDosis(LocalDate.of(2015, 8, 6)));

		PN pn3 = new PN(LocalDate.of(2015, 8, 10), LocalDate.of(2015, 8, 20), Paracetamol, 3);
		assertFalse(pn3.givDosis(LocalDate.of(2015, 8, 28)));

	}

}
